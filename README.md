[![build status](https://gitlab.esrf.fr/bliss/bliss/badges/master/pipeline.svg)](https://gitlab.esrf.fr/bliss/bliss/pipelines/master/latest)
<!-- [![coverage report](https://gitlab.esrf.fr/bliss/bliss/badges/master/coverage.svg)](https://bliss.gitlab-pages.esrf.fr/bliss/master/htmlcov) -->


# Documentation

Latest documentation: **https://bliss.gitlab-pages.esrf.fr/bliss/master**

# How to install

BLISS has many dependencies. Most notably it requires few additional, non-Python dependencies like the Redis server, which can't be install via pip.

For flexibility, we provide BLISS as a pure python package, **plus** a list of non-python requirements. These requirements can be installed by various means. Among those is Conda, which is the one used in development. Thus, it is the most guided way to install everything.

## With conda (or mamba):

**Easy way:**

Use the Makefile helper to install/update the environment for you:

```shell
make # and follow instructions ...
```

**Or manual:**

```shell
# Create a conda environment
conda create --name bliss_env
conda activate bliss_env
conda config --env --add channels conda-forge
conda config --env --append channels esrf-bcu

# Install non-python dependencies (and specify the python version you want)
conda install --file conda-requirements.txt python=3.*

# install bliss directly from sources (with his little brother blissdata)
pip install -e . -e ./blissdata
# ---- OR ---- from PyPI
pip install bliss
```

## Without conda:

For users who don't want to use Conda, they can install the dependencies provided in **conda-requirements.txt** on their system, a docker, etc; with the package manager they want.

```shell
# Install the dependencies in conda-requirements.txt on your own
# ...

# install bliss directly from sources (with his little brother blissdata)
pip install -e . -e ./blissdata
# ---- OR ---- from PyPI
pip install bliss
```
