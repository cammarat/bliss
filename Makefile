.ONESHELL: # Do not execute each line in a separate shell
SHELL := /bin/bash
CONDA_INIT := . $$(conda info --base)/etc/profile.d/conda.sh
CYAN := \033[36m
YELLOW := \033[33m
GREEN := \033[32m
END := \033[m

PYTHON_VERSION := 3.9
LIMA_SIMULATOR_CONDA_ENV := bliss_lima_simulator

all: help

help:
	@ # '@' begins each target to silence makefile
	printf "${CYAN}#############################################################################${END}\n"
	printf "MAKEFILE TO CREATE AND CONFIGURE CONDA ENVIRONMENTS FOR YOU.\n"
	printf "\n"
	printf "IMPORTANT: All commands are LAZY, no package is updated unless it is strictly\n"
	printf "required. It is up to you to update a particular package, or to start a fresh\n"
	printf "environment to get most recent version of everything.\n"
	printf "\n"
	printf "COMMANDS:\n"
	printf "    ${YELLOW}make bl_env NAME=<your_env_name>${END}\n"
	printf "        (typical ESRF beamline installation)\n"
	printf "        - Create a conda environment for BLISS with the name you provide.\n"
	printf "        - Common dependencies are installed (no dev specific ones).\n"
	printf "        - Bliss and blissdata are installed from sources (pip -e).\n"
	printf "    ${YELLOW}make dev_env NAME=<your_blissenv_name>${END}\n"
	printf "        (use this command to set up an ideal environment to develop with BLISS)\n"
	printf "        - Same as bl_env, but comes with dev dependencies like black or\n"
	printf "          pytest.\n"
	printf "        - Create another conda environment for lima simulator and store its\n"
	printf "          name in \$$LIMA_SIMULATOR_CONDA_ENV in the bliss environment.\n"
	printf "          Pytest will find it if blissenv is activated.\n"
	printf "    ${YELLOW}make demo_env NAME=<your_env_name>${END}\n"
	printf "        - Create an environment with bliss and blissdata packaged (no 'pip -e').\n"
	printf "        - Create another environment for lima simulator (like dev_env).\n"
	printf "        - Install demo plugins in the lima simulator environment.\n"
	printf "${CYAN}#############################################################################${END}\n"


_check_mamba:
	@which mamba > /dev/null || ( printf \
	"${YELLOW}This makefile use Mamba, a drop-in replacement for Conda, but faster.\n\
	Please install it with:\n\
	    conda install mamba -n base -c conda-forge\n${END}" && exit 1 )


_spawn_bliss_env: _check_mamba
	@$(CONDA_INIT)
ifndef NAME
	$(error NAME is not set, usage: make <target> NAME=<your_env>)
endif
	printf "${CYAN}Ensure \"${NAME}\" env exists${END}\n"
	conda activate $(NAME) 2> /dev/null || ( \
	 	printf "${YELLOW}Creating empty ${NAME} environment...${END}\n"; \
		mamba create --name ${NAME} --quiet --no-banner )
	printf "${YELLOW}Done.${END}\n"


_spawn_lima_env: _check_mamba
	@$(CONDA_INIT)
	printf "${CYAN}Ensure \"${LIMA_SIMULATOR_CONDA_ENV}\" env exists${END}\n"
	conda activate $(LIMA_SIMULATOR_CONDA_ENV) 2> /dev/null || ( \
	 	printf "${YELLOW}Creating ${LIMA_SIMULATOR_CONDA_ENV} environment...${END}\n"; \
		mamba env create --name ${LIMA_SIMULATOR_CONDA_ENV} -f environment-lima-simulator.yml --quiet )
	###############################################
	# INSTALL ADDITIONAL, SPECIFIC DEMO CODE
	conda run -n ${LIMA_SIMULATOR_CONDA_ENV} pip install -e ./bliss_lima_simulators
	###############################################
	printf "${YELLOW}Done.${END}\n"


bl_env: _spawn_bliss_env _check_mamba
	@$(CONDA_INIT)
	conda activate $(NAME)
	printf "${CYAN}Working environment:${END} $$CONDA_PREFIX\n"
	printf "${YELLOW}Installing conda dependencies in ${NAME}...${END}\n"
	mkdir -p $$CONDA_PREFIX/etc/conda/activate.d
	echo "export LIMA_SIMULATOR_CONDA_ENV=${LIMA_SIMULATOR_CONDA_ENV}" >> $$CONDA_PREFIX/etc/conda/activate.d/lima_simulator_env.sh
	# INSTALL CONDA DEPS ###########################
	mamba install --yes --no-banner \
		--file conda-requirements.txt \
		python=${PYTHON_VERSION} \
		-c esrf-bcu \
		-c conda-forge || exit 1
	###############################################
	printf "${YELLOW}Done.${END}\n"
	printf "${CYAN}Python version:${END} $$(python --version)\n"
	printf "${YELLOW}Installing bliss and blissdata from sources...${END}\n"
	# INSTALL PYPI PACKAGES #######################
	pip install \
		-e . \
		-e ./blissdata || exit 1
	###############################################
	printf "${YELLOW}Done.${END}\n"
	printf "${GREEN}DON'T FORGET TO ACTIVATE YOUR ENV:\n    conda activate ${NAME}${END}\n"


dev_env: _spawn_bliss_env _spawn_lima_env _check_mamba
	@$(CONDA_INIT)
	conda activate $(NAME)
	printf "${CYAN}Working environment:${END} $$CONDA_PREFIX\n"
	printf "${YELLOW}Installing conda dependencies in ${NAME} env...${END}\n"
	mkdir -p $$CONDA_PREFIX/etc/conda/activate.d
	echo "export LIMA_SIMULATOR_CONDA_ENV=${LIMA_SIMULATOR_CONDA_ENV}" >> $$CONDA_PREFIX/etc/conda/activate.d/lima_simulator_env.sh
	# INSTALL CONDA DEPS ###########################
	mamba install --yes --no-banner \
		--file conda-requirements.txt \
		--file conda-requirements-dev.txt \
		python=${PYTHON_VERSION} \
		-c esrf-bcu \
		-c conda-forge || exit 1
	###############################################
	printf "${YELLOW}Done.${END}\n"
	printf "${CYAN}Python version:${END} $$(python --version)\n"
	printf "${YELLOW}Installing bliss[dev] and blissdata[dev] from sources...${END}\n"
	# INSTALL PYPI PACKAGES #######################
	pip install \
		-e .[dev] \
		-e ./blissdata[dev] || exit 1
	###############################################
	printf "${YELLOW}Done.${END}\n"
	printf "${GREEN}DON'T FORGET TO ACTIVATE YOUR ENV:\n    conda activate ${NAME}${END}\n"


demo_env: _spawn_bliss_env _spawn_lima_env _check_mamba
	@$(CONDA_INIT)
	conda activate $(NAME)
	printf "${CYAN}Working environment:${END} $$CONDA_PREFIX\n"
	printf "${YELLOW}Installing conda dependencies in ${NAME} env...${END}\n"
	mkdir -p $$CONDA_PREFIX/etc/conda/activate.d
	echo "export LIMA_SIMULATOR_CONDA_ENV=${LIMA_SIMULATOR_CONDA_ENV}" >> $$CONDA_PREFIX/etc/conda/activate.d/lima_simulator_env.sh
	# INSTALL CONDA DEPS ###########################
	mamba install --yes --no-banner \
		--file conda-requirements.txt \
		--file conda-requirements-dev.txt \
		python=${PYTHON_VERSION} \
		-c esrf-bcu \
		-c conda-forge || exit 1
	###############################################
	printf "${YELLOW}Done.${END}\n"
	printf "${CYAN}Python version:${END} $$(python --version)\n"
	printf "${YELLOW}Installing bliss, blissdata and specific demo code...${END}\n"
	# INSTALL PYPI PACKAGES #######################
	pip install \
		. \
		./blissdata || exit 1
	###############################################
	printf "${YELLOW}Done.${END}\n"
	printf "${GREEN}DON'T FORGET TO ACTIVATE YOUR ENV:\n    conda activate ${NAME}${END}\n"
	printf "${GREEN}Then start demo servers in another terminal and follow the instructions:\n"
	printf "    ./demo/start_demo_servers.py \$$LIMA_SIMULATOR_CONDA_ENV\n"

.PHONY: _check_mamba _spawn_bliss_env _spawn_lima_env bl_env dev_env demo_env
