# Installation outside ESRF beamlines

BLISS has many dependencies. Most notably it requires few additional, non-Python dependencies like the Redis server, which can't be install via pip.

For flexibility, we provide BLISS as a pure python package, **plus** a list of non-python requirements. These requirements can be installed by various means. Among those is Conda, which is the one used in development. Thus, it is the most guided way to install everything.

The first step is to clone the [BLISS git repository](https://gitlab.esrf.fr/bliss/bliss):

```bash
git clone https://gitlab.esrf.fr/bliss/bliss
cd bliss/
```

## With conda (or mamba)

**Guided way:**

```bash
make # and follow instructions ...
```

**Or manual:**

```bash
# Create a conda environment
conda create --name bliss_env
conda activate bliss_env
conda config --env --add channels conda-forge
conda config --env --append channels esrf-bcu

# Install non-python dependencies (and specify the python version you want)
conda install --file conda-requirements.txt python=3.*

# install bliss directly from sources (with his little brother blissdata)
pip install -e . -e ./blissdata
# ---- OR ---- from PyPI
pip install bliss
```

## Without conda

For users who don't want to use Conda, they can install the dependencies provided in **conda-requirements.txt** on their system or within a docker with the package manager they want.

```shell
# Install the dependencies in conda-requirements.txt on your own
# ...

# Install bliss from PyPI
pip install bliss
# ---- OR ---- directly from sources (with his little brother blissdata)
pip install -e . -e ./blissdata
```

## Nexus writer

A TANGO server referred to as the *Nexus writer* can be [configured and started](data/config_nexus_writer.md#summary) if needed.

!!! warning
    There must be **one Nexus writer device** per BLISS session. Do not
    forget to add a device when a new BLISS session is created.

## Demo environment

An extra conda environment is needed for the *demo* environment

```bash
# Create a conda environment
conda config --env --add channels conda-forge
conda config --env --append channels esrf-bcu
conda create --name lima_simul_env --file environment-lima-simulator.yml
conda activate lima_simul_env

# Install lima plugins
pip install ./bliss_lima_simulators
```

Start the demo servers

```bash
conda activate bliss_env
python demo/start_demo_servers.py --lima-environment=lima_simul_env
```
