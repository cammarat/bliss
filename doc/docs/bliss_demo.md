## Starting BLISS demo processes

BLISS demonstration processes can be started with a script available in the BLISS source code repository,
within an environment corresponding to the testing environment (see [Test setup](dev_testing.md#Test setup))

```bash
cd <bliss_source_directory>
./demo/start_demo_servers.py
```

The script tells the command line to start the BLISS demo session:

```
#########################################################################################################
#                                 Start BLISS in another Terminal using                                 #
#                                                                                                       #
# > DEMO_ROOT=/home/bliss/demo TANGO_HOST=dagobah:10000 BEACON_HOST=dagobah:10001 bliss -s demo_session #
#                                                                                                       #
#                                   Press CTRL+C to quit this process                                   #
#########################################################################################################
```
