# Keithley 6487 configuration

Keithley 6487 is like the 6485 model with an **extra voltage source**.

## Yaml sample configuration
```YAML
plugin: keithley
keithleys:
  - model: 6487
    auto_zero: False
    display: True
    gpib:
     #  url: enet://gpibcb184a
      url: tango_gpib_device_server://id00/gpib/l-cb182-1      
      pad: 11
    sensors:
      - name: kipoolci 
        address: 1
        nplc: 0.1
        zero_check: False
        zero_correct: False


```
# Add in session_name_setup.py a soft axis *keith_HV_SR*

keith_HV_SR = SoftAxis('kipool_source_voltage', kipoolci, position='source_value', move='source_value')

# Shell info example:
```python
SPECTRO [1]: keith_HV_SR
    Out [1]: AXIS:
                   name (R): kipool_source_voltage
                   unit (R): None
                   offset (R): 0.00000
                   backlash (R): 0.00000
                   sign (R): 1
                   steps_per_unit (R): 1.00
                   tolerance (R) (to check pos. before a move): 0.0001
                   limits (RW):    Low: -inf High: inf    (config Low: -inf High: inf)
                   dial (RW): -88.00000
                   position (RW): -88.00000
                   state (R): READY (Axis is READY)
                   acceleration: None
                   velocity: None
 
SPECTRO [2]: wm(keith_HV_SR)

            kipool_source_voltage
--------    -----------------------
User
 High	inf
 Current    -88.00000
 Low	-inf
Offset  0.00000

Dial
 High	inf
 Current    -88.00000
 Low	-inf

SPECTRO [3]: umv(keith_HV_SR,-87)
Moving kipool_source_voltage from -88 to -87

kipool_source_voltage
user	-87.000
dial	-87.000

SPECTRO [4]: kipoolci.source_range = -250

SPECTRO [5]: kipoolci.source_range
Out [5]: -250

SPECTRO [6]: kipoolci.source_enable = 1

SPECTRO [7]: kipoolci.source_enable
Out [7]: 1
```


