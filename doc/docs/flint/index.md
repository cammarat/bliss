# What is Flint

Flint is a GUI companion for BLISS for online data visualization.

It built based on [silx][1] (ScIentific Library for eXperimentalists)
and [Qt][2] (Qt).

It mostly provides live display of BLISS scans, custom plotting which can be
written in BLISS scripts and few graphic helpers.

## Live display of scans

Flint listens to scan data source to know if there's something to display.
The chart type is automatically determined using information stored in
the scan (we talk about scan info).

The data display is updated in real time following the update of our database
Redis, and the refresh rate enforced by the user for each plots.

Here is the list of supported charts

- [Curve](live_curve.md)
- [Image](live_image.md)
- [Scatter](live_scatter.md)
- [MCA](live_mca.md)
- [Other live data](other_live_display.md)

## Custom plotting

Flint provides to BLISS an API to create and interact with charts remotely.

- [User data plotting](flint_data_plotting.md)
- [Interactive selection of region](flint_interaction.md)

## Automatic start

When Flint is running, it will automatically display live scans.

If the session is configured for (which is the case for the demo session),
Flint will be started by the invocation of any standard scan command.

It can be enabled this way:

```python
SCAN_DISPLAY.auto = True
```

## APIs

Here is the available commands which can be used from the BLISS shell.

### Start and stop

From BLISS shell, few basic interaction with Flint are provided.

To invoke or retrieve a proxy to interact with Flint you can use:

```python
flint()
```

!!! note
     If there is a fail to find or to create Flint, this method will raise an
     exception.

The returned object provides few methods to close the application:

```python
f = flint()
f.close()    # request a normal close of the application
f.kill()     # kill
f.kill9()    # kill -9
```

### Set the focus

If your window manager provides this feature, you can force the focus on the
application this way:

```python
f = flint()
f.focus()
```

[1]: https://silx.org
[2]: https://www.qt.io/
