# Interactive data selection

In order to interact with a given plot, to choose points or regions of interest
of different shapes, plot objects propose two methods : `select_point()` and
`select_shape()`.

Most of the available charts provided by Flint provides such interactions.

## Get the plot

Here is a small example creating a chart based on fixed data.

```python
from bliss.common.plot import *
import numpy

xx = numpy.linspace(0, 4*3.14, 50)
yy = numpy.cos(xx)

p = plot(yy, name="Plot 0")
```

`p` refers to a plot in Flint and provides it's own method to interact with.

## Data selection

Once that the plot `p` object is created (an `CurvePlot` in this case), several
options are available to interact with the plot:

- `select_points(pointnumber)`:
- `select_shape(shape)` where `shape` could be:
     - `'rectangle'`: rectangle selection
     - `'line'`: line selection
     - `'hline'`: horizontal line selection
     - `'vline'`: vertical line selection
     - `'polygon'`: polygon selection

!!! note
     When selecting a ``polygon`` shape, click on the starting point of the
     polygon to close it (the polygon) and to return to BLISS shell.

### Select a rectangle

```python
rectangle = p.select_shape("rectangle")
```

BLISS shell is blocked until user makes a rectangular selection.

Once the rectangle is created, the interaction is terminated, and the result is
passed to the BLISS shell.

```python
rectangle
Out [11]: [[278.25146, 716.00623], [623.90546, 401.82913]]
```

### Select a list of points

The ``select_points`` method allows the user to select a given number of point
on the corresponding plot using their mouse.

```python
a, b, c = p.select_points(3)
# Blocks until the user selects the 3 points
a
[1.2, 3.4]
```

### Other selection kind

```python
topleft, bottomright = p.select_shape('rectangle')

start, stop = p.select_shape('line')

left, right = p.select_shape('hline')

bottom, top = p.select_shape('vline')

points = p.select_shape('polygon')
```
