# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
from silx.gui import qt
from silx.gui.utils.testutils import TestCaseQt


@pytest.fixture(scope="session")
def qapp(xvfb):
    app = qt.QApplication.instance()
    if app is None:
        app = qt.QApplication([])
    try:
        yield app
    finally:
        if app is not None:
            app.closeAllWindows()


@pytest.fixture(scope="module")
def silxTestUtilsModule():
    utils = TestCaseQt()
    utils.setUpClass()
    try:
        yield utils
    finally:
        utils.tearDownClass()


@pytest.fixture
def silxTestUtils(silxTestUtilsModule):
    silxTestUtilsModule.setUp()
    try:
        yield silxTestUtilsModule
    finally:
        silxTestUtilsModule.tearDown()
