"""Testing LogWidget."""

import logging
import weakref
import pytest
from bliss.flint.widgets.logging_list import LoggingList
from bliss.flint.model.logging_model import LoggingModel

test_logger = logging.getLogger(__name__)
test_logger.propagate = False


@pytest.fixture
def loggingList(silxTestUtils):
    widget = LoggingList()
    silxTestUtils.qWaitForWindowExposed(widget)
    model = LoggingModel()
    model.connectLogger(test_logger)
    widget.setLogModel(model)
    try:
        yield widget
    finally:
        widget.setLogModel(None)
        model.disconnectAll()

        ref = weakref.ref(widget)
        widget = None
        silxTestUtils.qWaitForDestroy(ref)

        ref = weakref.ref(model)
        model = None
        silxTestUtils.qWaitForDestroy(ref)


def test_logging(xvfb, loggingList):
    widget = loggingList
    assert widget.logCount() == 0
    test_logger.warning("Tout le %s s'eclate", "monde")
    widget.flushRecords()
    test_logger.error("A la queu%s%s", "leu", "leu")
    widget.flushRecords()
    assert widget.logCount() == 2
    widget = None


@pytest.mark.usefixtures("xvfb")
def plain_text(widget):
    model = widget.model()
    text = ""
    for i in range(model.rowCount()):
        index = model.index(i, widget.MessageColumn)
        item = model.itemFromIndex(index)
        if text != "":
            text += "\n"
        text += item.text()
    return text


def test_buggy_logging(xvfb, silxTestUtils, loggingList):
    widget = loggingList
    assert widget.logCount() == 0
    test_logger.warning("Two fields expected %s %f", "foo")
    test_logger.warning("Float field expected %f", "foo")
    widget.flushRecords()
    assert widget.logCount() == 2


def test_causes(xvfb, silxTestUtils, loggingList):
    """Coverage with exception containing causes"""
    widget = loggingList
    try:
        try:
            try:
                raise IndexError("AAA")
            except Exception as e:
                raise RuntimeError("BBB") from e
        except Exception as e:
            raise RuntimeError("CCC") from e
    except Exception as e:
        test_logger.critical("Hmmmm, no luck", exc_info=e)

    silxTestUtils.qWait()
    widget.flushRecords()
    assert widget.logCount() == 1

    model = widget.model()
    index = model.index(0, 0)
    assert model.rowCount(index) == 1
    index = model.index(0, 0, index)
    assert model.rowCount(index) == 1
    index = model.index(0, 0, index)
    assert model.rowCount(index) == 1
    index = model.index(0, 0, index)
    assert model.rowCount(index) == 0
