"""Testing for profile action"""

import numpy
from bliss.flint.model import scan_model
from bliss.flint.widgets.utils import flat_field_correction


def test_correction():
    correction = flat_field_correction.FlatFieldCorrection()
    flat = numpy.array([[2, 2, 2, 3]])
    dark = numpy.array([[1, 1, 2, 1]])
    data = numpy.array([[11, 21, 11, 11]])
    correction.setDark(dark, 1)
    correction.setFlat(flat, 1)
    result = correction.correction(data, exposureTime=1)
    expected = numpy.array([[10, 20, 0, 5]])
    numpy.testing.assert_almost_equal(result, expected)


def test_exposure():
    correction = flat_field_correction.FlatFieldCorrection()
    flat = numpy.array([[1, 1, 1, 1.5]])
    dark = numpy.array([[0.25, 0.25, 0.5, 0.25]])
    data = numpy.array([[22, 42, 22, 22]])
    correction.setDark(dark, 0.25)
    correction.setFlat(flat, 0.5)
    result = correction.correction(data, exposureTime=2)
    expected = numpy.array([[10, 20, 0, 5]])
    numpy.testing.assert_almost_equal(result, expected)


def test_wrong_dark_shape():
    correction = flat_field_correction.FlatFieldCorrection()
    flat = numpy.array([[2, 2, 0, 3]])
    dark = numpy.array([[1, 1, 2]])
    data = numpy.array([[20, 40, 11, 15]])
    correction.setDark(dark, 1)
    correction.setFlat(flat, 1)
    result = correction.correction(data, exposureTime=1)
    expected = numpy.array([[10, 20, 0, 5]])
    numpy.testing.assert_almost_equal(result, expected)


def test_wrong_flat_shape():
    correction = flat_field_correction.FlatFieldCorrection()
    flat = numpy.array([[2, 2, 0]])
    dark = numpy.array([[1, 1, 11, 1]])
    data = numpy.array([[11, 21, 11, 6]])
    correction.setDark(dark, 1)
    correction.setFlat(flat, 1)
    result = correction.correction(data, exposureTime=1)
    expected = numpy.array([[10, 20, 0, 5]])
    numpy.testing.assert_almost_equal(result, expected)


def test_read_tomo_flat_scan():
    scan = scan_model.Scan()
    scanInfo = {
        "technique": {"flat": {"exposure_time": 2.0, "exposure_time@units": "s"}}
    }
    scan.setScanInfo(scanInfo)
    correction = flat_field_correction.FlatFieldCorrection()
    data = numpy.array([[11, 21, 11, 6]])
    correction.captureImage(scan, data)
    assert correction.flat().exposureTime == 2


def test_read_tomo_dark_scan():
    scan = scan_model.Scan()
    scanInfo = {
        "technique": {"dark": {"exposure_time": 2.0, "exposure_time@units": "s"}}
    }
    scan.setScanInfo(scanInfo)
    correction = flat_field_correction.FlatFieldCorrection()
    data = numpy.array([[11, 21, 11, 6]])
    correction.captureImage(scan, data)
    assert correction.dark().exposureTime == 2
