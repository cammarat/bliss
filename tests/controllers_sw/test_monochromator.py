# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest

from bliss.common.standard import mv, info


def approx(val, delta=0.0005):
    return pytest.approx(val, delta)


EXPECTED_TRACK_OFF = """              enetrack         ene    sim_c1    sim_c2    sim_acc
Calculated  18.592 keV  18.592 keV  1.425 mm  1.423 mm  -0.096 mm
   Current  18.592 keV  18.592 keV  0.000 mm  0.000 mm   0.000 mm
  Tracking                               OFF       OFF        OFF
"""

EXPECTED_TRACK_ON = """              enetrack         ene    sim_c1    sim_c2    sim_acc
Calculated  18.592 keV  18.592 keV  1.425 mm  1.423 mm  -0.096 mm
   Current  18.592 keV  18.592 keV  0.000 mm  0.000 mm   0.000 mm
  Tracking                                ON       OFF        OFF
"""


@pytest.fixture
def mono_fix_exit(beacon):
    bragg = beacon.get("sim_mono")
    bragg.move(10)
    mono = beacon.get("simul_mono_fix_exit")
    mono.xtal.change("Si220")
    yield mono
    mono._close()


def test_fix_exit(beacon, mono_fix_exit):
    # for the moment, it is enough to check if the class can be instantiated
    _ = beacon.get("ene")
    info(mono_fix_exit)


def test_bragg2energy(simulation_monochromator):
    mono = simulation_monochromator
    assert mono.bragg2energy(13.333) == approx(14)
    assert mono.energy2bragg(14) == approx(13.333)
    assert mono.energy2bragg(15) == approx(12.429)


def test_mono_xtals(simulation_monochromator):
    """Crystals manager"""
    mono = simulation_monochromator
    info(mono)

    xtals = mono._xtals
    assert mono._xtal_is_in("Si220")

    mono.xtal.change(xtals.xtal_names[0])

    assert mono._xtals.xtal_sel == xtals.xtal_names[0]


def test_mono_tracking_parameters(beacon, simulation_monochromator):
    mot = beacon.get("sim_c1")

    mot.tracking.param_id.harmonic_2()
    mot.tracking.mode.set("polynom")

    with pytest.raises(ValueError):
        mot.tracking.mode.set("toto")


def test_mono_tracking_onoff(beacon, simulation_monochromator):
    mono = simulation_monochromator
    sim_c1 = beacon.get("sim_c1")

    sim_c1.tracking.off()
    assert sim_c1.tracking.state is False
    info = mono._info_motor_tracking()
    assert info == EXPECTED_TRACK_OFF

    sim_c1.tracking.on()
    assert sim_c1.tracking.state is True
    info = mono._info_motor_tracking()
    assert info == EXPECTED_TRACK_ON

    mono.tracking.all_off()
    for axis in mono.tracking._motors.values():
        assert axis.tracking.state is False
    mono.tracking.all_on()
    for axis in mono.tracking._motors.values():
        assert axis.tracking.state is True

    mono.tracking.all_mode("table")
    for axis in mono.tracking._motors.values():
        assert axis.tracking.mode.get() == "table"


def test_mono_tracking_move(beacon, simulation_monochromator):
    mono = simulation_monochromator
    c1 = beacon.get("sim_c1")
    ene = mono.motors["energy"]
    enetrack = mono.motors["energy_tracker"]
    bragg = mono.motors["bragg"]

    def calculated_position(mot):
        return mot.tracking.energy2tracker(ene.position)

    mono.tracking.all_off()

    mv(ene, 14)
    assert bragg.position == approx(13.333)
    assert ene.position == approx(14)
    assert enetrack.position == approx(14)

    mv(bragg, 10)
    assert ene.position == approx(18.593)
    assert enetrack.position == approx(18.593)

    c1.tracking.on()

    mv(enetrack, 14)
    assert bragg.position == approx(13.333)
    assert ene.position == approx(14)
    assert enetrack.position == approx(14)
    assert calculated_position(c1) == approx(1.107)
    assert c1.position == approx(1.107)


def test_dataset_metadata(simulation_monochromator):
    mono = simulation_monochromator
    mdata = mono.dataset_metadata()
    expected = {
        "crystal": {
            "d_spacing": approx(1.92e-10),
            "reflection": (2, 2, 0),
            "type": "Si",
        },
        "energy": approx(18.5919),
        "name": "simul_mono",
        "wavelength": approx(6.6686e-11),
    }
    assert mdata == expected

    mv(mono.motors["energy"], 14)
    mdata = mono.dataset_metadata()

    expected = {
        "crystal": {
            "d_spacing": approx(1.92e-10),
            "reflection": (2, 2, 0),
            "type": "Si",
        },
        "energy": approx(14),
        "name": "simul_mono",
        "wavelength": approx(8.856e-11),
    }
    assert mdata == expected


def test_scan_metadata(simulation_monochromator):
    mono = simulation_monochromator
    mdata = mono.scan_metadata()
    expected = {
        "@NX_class": "NXmonochromator",
        "crystal": {
            "@NX_class": "NXcrystal",
            "d_spacing": approx(1.92e-10),
            "d_spacing@units": "m",
            "reflection": (2, 2, 0),
            "type": "Si",
        },
        "energy": approx(18.5919),
        "wavelength": approx(6.6686e-11),
        "energy@units": "keV",
        "wavelength@units": "m",
    }
    assert mdata == expected

    mv(mono.motors["energy"], 14)
    mdata = mono.scan_metadata()

    expected = {
        "@NX_class": "NXmonochromator",
        "crystal": {
            "@NX_class": "NXcrystal",
            "d_spacing": approx(1.92e-10),
            "d_spacing@units": "m",
            "reflection": (2, 2, 0),
            "type": "Si",
        },
        "energy": approx(14),
        "wavelength": approx(8.856e-11),
        "energy@units": "keV",
        "wavelength@units": "m",
    }
    assert mdata == expected


@pytest.fixture
def mono_mlayer(beacon):
    bragg = beacon.get("sim_mono")
    bragg.move(0.5)
    mono = beacon.get("simul_mlmono")
    mono.xtal.change("Ru")
    yield mono
    mono._close()


def test_mono_mlayer_move(beacon, mono_mlayer):
    mono = mono_mlayer
    ene = beacon.get("ene")
    bragg = mono.motors["bragg"]

    ene.move(14)
    assert bragg.position == approx(0.6583)

    mv(bragg, 0.9)
    assert ene.position == approx(10.240)


def test_mono_mlayer_xtal(beacon, mono_mlayer):
    mono = mono_mlayer
    ene = mono.motors["energy"]
    bragg = mono.motors["bragg"]
    mly = beacon.get("sim_mly")
    mlz = beacon.get("sim_mlz")

    mono.xtal.change("W")

    # check xtal change taken into account the new energy, with Ru energy was 18.43
    # now is 17.055 keV
    assert mono._xtal_is_in("W")
    assert ene.position == approx(17.4055)

    # check bragg offset is applied
    assert bragg.offset == 0.3
    # check trans. motors have been moved
    assert mly.position == approx(0.8)
    assert mlz.position == approx(40.0)

    # change the energy and check the bragg angle
    mv(ene, 20)
    assert bragg.position == approx(0.6964)
