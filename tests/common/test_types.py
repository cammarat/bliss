import pytest
from numpy import uint8, float64
from bliss.common import scans


def test_type_checking(default_session):
    m0 = default_session.config.get("m0")
    diode = default_session.config.get("diode")

    for intval, floatval in [(8, 0.2), (uint8(8), 0.2), (8, float64(0.2))]:
        scans.ascan(m0, 1.2, 4.0, intval, floatval, diode, run=False)

    with pytest.raises(TypeError) as e_info:
        scans.ascan()
    assert "missing 5 required positional arguments" in str(e_info.value)

    with pytest.raises(TypeError) as e_info:
        scans.ascan(m0)
    assert "missing 4 required positional arguments" in str(e_info.value)

    with pytest.raises(TypeError) as e_info:
        scans.ascan(42)
    assert "parameter motor=42 violates type hint" in str(e_info.value)
