# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import sys
import multiprocessing
from gevent.monkey import is_anything_patched
from blissdata.data.remote_node import RemoteNodeWalker
from blissdata.data.events import EventType

# IMPORTANT NOTE: Do not import bliss modules there, if you need some, do it inside your test case.
# This is because subprocesses are spawned in this file, and they should not import anything from bliss
# to stay gevent-free.


def subproc_walk_on_new_event(
    mp_queue, beacon_host, beacon_port, db_name, started_event
):
    assert not is_anything_patched()

    root_node = RemoteNodeWalker(beacon_host, beacon_port, db_name)
    for event in root_node.walk_on_new_events(started_event=started_event):
        assert not is_anything_patched()
        mp_queue.put(event.type)
        if event.type is EventType.END_SCAN:
            sys.exit(0)


def test_walk_on_new_event_no_gevent(beacon_host_port, default_session):
    from bliss.common import scans

    assert is_anything_patched()

    # run a first scan to make redis session node appear
    diode = default_session.config.get("diode")
    scans.loopscan(1, 0.1, diode, save=True)

    # Use spawn instead of fork to not inherit gevent patching of the parent process.
    # Do it in a context to not change the start method for the whole pytest process.
    ctx = multiprocessing.get_context("spawn")

    mp_queue = ctx.Queue()
    started_event = ctx.Event()
    beacon_host, beacon_port = beacon_host_port
    proc = ctx.Process(
        target=subproc_walk_on_new_event,
        args=(mp_queue, beacon_host, beacon_port, default_session.name, started_event),
    )
    proc.start()
    try:
        assert started_event.wait(timeout=10)

        scans.loopscan(2, 0.1, diode, save=True)

        while True:
            ev_type = mp_queue.get(timeout=10)
            if ev_type is EventType.END_SCAN:
                break

        proc.join(timeout=10)
        assert not proc.is_alive()
        assert proc.exitcode == 0
    finally:
        proc.terminate()
        proc.join()
