# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import click
import pytest

from click.testing import CliRunner

from bliss.shell.getval import getval_yes_no, getval_name, getval_int_range
from bliss.shell.getval import getval_idx_list, getval_char_list


@pytest.fixture(scope="function")
def runner(request):
    return CliRunner()


@click.command()
def function_getval_yes_no():
    ans1 = getval_yes_no("do you want", default=False)
    print(f"ans1={ans1}")

    ans2 = getval_yes_no("do you want", default=False)
    print(f"ans2={ans2}")

    ans3 = getval_yes_no("do you want", default=False)
    print(f"ans3={ans3}")

    ans4 = getval_yes_no("do you want", default=False)
    print(f"ans4={ans4}")


def test_getval_yes_no(runner):
    result = runner.invoke(
        function_getval_yes_no, input="\n".join(["rrr", "yes", "YES", "NO", "yeS"])
    )
    assert not result.exception
    # print("\n+++++++++++++ result.output +++++++++++\n" + result.output + "\n++++++++++++++++++++++++\n")
    # print(dir(result)) # exc_info', 'exception', 'exit_code', 'output', 'return_value', 'runner', 'stderr', 'stderr_bytes', 'stdout', 'stdout_bytes'
    assert "Error: invalid input" in result.output
    assert "ans1=True" in result.output
    assert "ans2=True" in result.output
    assert "ans3=False" in result.output
    assert "ans4=True" in result.output


@click.command()
def function_getval_name():
    ans1 = getval_name("Enter name")
    print(f"ans1={ans1}")


def test_getval_name(runner):
    result = runner.invoke(function_getval_name, input="1toto2\ntiti")
    assert not result.exception
    # print("\n+++++++++++++ result.output +++++++++++\n" + result.output + "\n++++++++++++++++++++++++\n")
    assert "Enter name: 1toto2" in result.output
    assert "Enter name: titi" in result.output
    assert "ans1=titi" in result.output


@click.command()
def function_getval_int_range():
    ans1 = getval_int_range("enter int", minimum=4, maximum=444)
    print(f"ans1={ans1}")


def test_getval_int_range(runner):
    result = runner.invoke(function_getval_int_range, input="333")
    assert not result.exception
    # print("\n+++++++++++++ result.output +++++++++++\n" + result.output + "\n++++++++++++++++++++++++\n")
    # print(result.output)
    assert "ans1=333" in result.output


@click.command()
def function_getval_idx_list():
    dspacing_list = ["111", "311", "642"]
    ans1 = getval_idx_list(dspacing_list, "enter value")
    print(f"ans1={ans1}")


def test_getval_idx_list(runner):
    result = runner.invoke(function_getval_idx_list, input="2")
    assert not result.exception
    # print("\n+++++++++++++ result.output +++++++++++\n" + result.output + "\n++++++++++++++++++++++++\n")
    # print(result.output)
    assert "ans1=(2, '311')" in result.output


@click.command()
def function_getval_char_list():
    actions_list = [("a", "add a roi"), ("r", "remove a roi"), ("m", "modify a roi")]
    ans1 = getval_char_list(actions_list, "Action to do")
    print(f"ans1={ans1}")


def test_getval_char_list(runner):
    result = runner.invoke(function_getval_char_list, input="a")
    assert not result.exception
    # print("\n+++++++++++++ result.output +++++++++++\n" + result.output + "\n++++++++++++++++++++++++\n")
    # print(result.output)
    assert "ans1=('a', 'add a roi')" in result.output
