#!/usr/bin/env python
import os
import sys
import pathlib
import pkg_resources
from setuptools.config import setupcfg

bliss_root_path = pathlib.Path(__file__).parent.parent

if sys.platform == "win32":
    conda_requirements_path = bliss_root_path / "conda-requirements-win.txt"
else:
    conda_requirements_path = bliss_root_path / "conda-requirements.txt"

# extracting setup.cfg information
setup_cfg = setupcfg.read_configuration(bliss_root_path / "setup.cfg")
install_requires = setup_cfg["options"]["install_requires"]
entry_points = setup_cfg["options"]["entry_points"]["console_scripts"]

# translation table for PyPI packages using a different name in Conda
pypi2conda = {"msgpack": "msgpack-python", "redis": "redis-py"}

# translate setup.cfg requirements to conda format
translated_pypi_reqs = []
for req in pkg_resources.parse_requirements(install_requires):
    # ignore "pyqt5" from PyPI because conda-requirements already has it with name "pyqt"
    if req.name == "pyqt5":
        continue
    if req.name in pypi2conda:
        req.name = pypi2conda[req.name]
    translated_pypi_reqs.append(str(req))

# Get conda-only requirements and python version defined by the CI job
conda_reqs = [f"python=={os.environ['PYTHON_VERSION']}"]
with open(conda_requirements_path, "r") as f:
    conda_reqs += [str(req) for req in pkg_resources.parse_requirements(f)]


def format_list(list):
    return "\n".join([f"    - {val}" for val in list])


recipe = f"""\
package:
  name: {setup_cfg['metadata']['name']}
  version: {setup_cfg['metadata']['version']}
source:
  path: ./..
about:
  home: {setup_cfg['metadata']['url']}
  license: {setup_cfg['metadata']['license']}
  license_family: LGPL
  summary: {setup_cfg['metadata']['description']}
build:
  pin_depends: strict
  noarch: python
  entry_points:
{format_list(entry_points)}
  number: {os.environ.get("CI_PIPELINE_ID", 0)}
  string: {"" if "GITLAB_CI" in os.environ else "dev"}
  script: python -m pip install .
test:
    imports:
        - bliss
        - nexus_writer_service
requirements:
  build:
    - python
  run:
    # Pure conda packages:
{format_list(conda_reqs)}

    # Packages translated from PyPI to conda:
{format_list(translated_pypi_reqs)}
"""

print(recipe)
