from bliss.setup_globals import (
    sy,
    sz,
    beamviewer,
    beam_shutter,
    sample_shutter,
    tomocam,
    ct,
)
from bliss import current_session
from bliss.controllers.lima.roi import Roi
from bliss.common.scans.simulation import simu_l2scan  # noqa: F401

sy.custom_set_measured_noise(0.002)
sz.custom_set_measured_noise(0.002)

# set run-level of roi to be higher that the one of simulator
beamviewer.roi_counters._proxy.Stop()
beamviewer.roi_counters._proxy.RunLevel = 100
beamviewer.roi_counters._proxy.Start()

# put a roi
r1 = Roi(0, 0, 1023, 1023)
beamviewer.roi_counters["roi1"] = r1


load_script("demo_session.py")  # noqa: F821
if "SCAN_DISPLAY" in current_session.env_dict:
    current_session.env_dict["SCAN_DISPLAY"].auto = False

print(
    """
Welcome to your new 'demo_session' BLISS session !!

You have a 1mm x 1mm sample mounted on a sample stage that can be moved with sy and sz

Some ideas for scans:

- amesh(sy,-.75,.75,30,sz,-.75,.75,30,.001,fluo_diode)
- amesh(sy,-.1,.1,20,sz,-.3,0,30,.001,fluo_diode)
- umv(slit_vertical_gap,.1);ascan(slit_vertical_offset,-1,1,30,.1,beamviewer)
- timescan(1, tomocam)
- timescan(1, diffcam)
- timescan(1, mca1)
- regulation.plot();sleep(1);regulation.setpoint=10
- s=simu_l2scan(robx,20,85,100, roby,50,110,30, 0.01, fluo_diode2)
- dark(); flat(): ct(tomocam)

Slits are fully open when slit_top=10 and slit_bottom=10
Slits are not aligned, yet!

"""
)


def dark(expotime=None):
    if expotime is None:
        expotime = 1
    with beam_shutter.closed_context:
        ct(
            tomocam,
            scan_info={
                "technique": {
                    "dark": {"exposure_time": expotime, "exposure_time@units": "s"}
                }
            },
        )


def flat(expotime=None):
    if expotime is None:
        expotime = 1
    with beam_shutter.open_context:
        with sample_shutter.closed_context:
            ct(
                tomocam,
                scan_info={
                    "technique": {
                        "flat": {"exposure_time": expotime, "exposure_time@units": "s"}
                    }
                },
            )
