import os
import time
import numpy
import h5py
import tango
import redis

from Lima import Simulator as LimaSimuMod
import Lima.Server.camera.Simulator as TangoSimuMod


class LimaTomoSimulator(LimaSimuMod.Camera):
    def __init__(self):
        LimaSimuMod.Camera.__init__(self)
        self._dark = None
        self._flat = None
        self._data = None
        self._data_filename = None
        self._loaded = False
        self.beam_shutter = None
        self.sample_shutter = None

    def set_data_filename(self, data_filename):
        self._data_filename = data_filename

    def init_img(self):
        """Initialize resources.

        It is not done during object construction to speed up
        the device initialization.
        """
        if self._loaded:
            return
        self._loaded = True
        with h5py.File(self._data_filename, mode="r") as f:
            self._dark = f["scan1/dark/data"][...].astype(float)
            self._flat = f["scan1/flat_000/data"][...].astype(float)
            self._data = f["scan1/instrument/data"][0].astype(float)

    def __get_shutter_state(self, redis_conn, device_name):
        value = redis_conn.hget(f"{device_name}:settings", "state")
        if b"Closed" in value:
            return "CLOSED"
        if b"Open" in value:
            return "OPEN"
        return None

    def fillData(self, data):
        """
        Callback function from the simulator
        """
        self.init_img()

        self.redis_conn = redis.Redis(
            host="localhost", port=int(os.environ["BEACON_REDIS_PORT"])
        )
        beam_state = self.__get_shutter_state(self.redis_conn, self.beam_shutter)
        sample_state = self.__get_shutter_state(self.redis_conn, self.sample_shutter)

        # Inverse ramp from 100% to 20% restarting back every 'duration' second
        duration = 20
        intensity = int(time.time()) % duration
        intensity = 0.1 + 0.9 * (duration - intensity) / duration

        if beam_state == "CLOSED":
            result = self._dark
        elif sample_state == "CLOSED":
            result = self._flat
        else:
            result = self._data

        image = numpy.random.poisson(result) * intensity

        # Clamp the image to the result data
        width = min(data.buffer.shape[0], image.shape[0])
        height = min(data.buffer.shape[1], image.shape[1])
        data.buffer[...] = 0
        data.buffer[0:width, 0:height] = image[0:width, 0:height]


class TomoSimulator(TangoSimuMod.Simulator):
    def init_device(self):
        TangoSimuMod.Simulator.init_device(self)
        self._SimuCamera.set_data_filename(self.data_filename)
        self._SimuCamera.beam_shutter = self.beam_shutter
        self._SimuCamera.sample_shutter = self.sample_shutter


class TomoSimulatorClass(TangoSimuMod.SimulatorClass):
    # Device Properties
    device_property_list = {
        "data_filename": [tango.DevString, "filename containing the data", []],
        "beam_shutter": [
            tango.DevString,
            "beam shutter to know if the beam is visible",
            [],
        ],
        "sample_shutter": [
            tango.DevString,
            "sample shutter to kniow if the sample is mounted",
            [],
        ],
    }

    device_property_list.update(TangoSimuMod.SimulatorClass.device_property_list)


def get_control(**kwargs):
    return TangoSimuMod.get_control(
        **kwargs, _Simulator=TomoSimulator, _Camera=LimaTomoSimulator
    )


def get_tango_specific_class_n_device():
    return TomoSimulatorClass, TomoSimulator
