# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations
from typing import Union
from typing import Optional

import logging
import weakref
import numpy

from silx.gui import qt
from silx.gui import icons
from silx.gui.widgets.FormGridLayout import FormGridLayout
from bliss.flint.model import flint_model
from bliss.flint.model import plot_model
from bliss.flint.model import plot_item_model
from bliss.flint.model import scan_model
from .eye_check_box import EyeCheckBox
from .cog_check_box import CogCheckBox
from .icon_widget import IconWidget
from .utils import plot_helper
from .utils import refresh_helper
from silx.math.combo import min_max

_logger = logging.getLogger(__name__)


class _DeviceField(qt.QLabel):
    def plotItem(self):
        plotItem = self.__plotItem
        if plotItem is None:
            return None
        return plotItem()

    def setPlotItem(self, plotItem: plot_item_model.ImageItem):
        if plotItem is None:
            self.setText("")
            self.setEnabled(False)
            return

        dataChannel = plotItem.imageChannel()
        if dataChannel is None:
            self.setText("")
            self.setEnabled(False)
            return

        deviceName, _channelName = dataChannel.name().rsplit(":", 1)
        self.setEnabled(True)
        self.setText(deviceName)


class _ImageField(qt.QWidget):
    def __init__(self, parent):
        super(_ImageField, self).__init__(parent=parent)
        self.__plotItem = None

        self.__label = qt.QLabel(self)
        self.__display = EyeCheckBox(self)
        self.__display.toggled.connect(self.__checked)
        layout = qt.QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.__label)
        layout.addStretch(1)
        layout.addWidget(self.__display)

    def plotItem(self):
        plotItem = self.__plotItem
        if plotItem is None:
            return None
        return plotItem()

    def __checked(self, isChecked):
        plotItem = self.plotItem()
        if plotItem is None:
            return
        plotItem.setVisible(isChecked)

    def setPlotItem(self, plotItem: plot_item_model.ImageItem):
        if plotItem is None:
            self.__plotItem = None
            self.__label.setText("")
            self.setEnabled(False)
            return

        self.__plotItem = weakref.ref(plotItem)

        self.__display.setChecked(plotItem.isVisible())

        dataChannel = plotItem.imageChannel()
        if dataChannel is None:
            self.__label.setText("")
            self.setEnabled(False)
            return

        self.setEnabled(True)
        channelName = dataChannel.name()
        self.__label.setText(channelName)


class _ImageSizeField(qt.QWidget):
    def __init__(self, parent):
        super(_ImageSizeField, self).__init__(parent=parent)
        self.__plotItem = None
        self.__scan = None

        self.__label = qt.QLabel(self)
        layout = qt.QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.__label)

    def plotItem(self):
        plotItem = self.__plotItem
        if plotItem is None:
            return None
        return plotItem()

    def setPlotItem(self, plotItem: plot_item_model.ImageItem):
        self.__plotItem = weakref.ref(plotItem)
        self.updateData()

    def scan(self):
        return self.__scan

    def setScan(self, scan: scan_model.Scan):
        self.__scan = scan
        self.updateData()

    def updateData(self):
        scan = self.__scan
        plotItem = self.plotItem()
        if scan is None or plotItem is None:
            self.__label.setText("-")
            return

        imageChannel = plotItem.imageChannel()
        if imageChannel is None:
            self.__label.setText("")
            return

        data = imageChannel.data(self.__scan)
        if data is None:
            self.__label.setText("")
            return

        array = data.array()
        if array is None:
            self.__label.setText("")
            return

        shape = array.shape
        title = " × ".join(reversed([str(x) for x in shape]))
        self.__label.setText(title)


class _ImageDTypeField(qt.QWidget):
    def __init__(self, parent):
        super(_ImageDTypeField, self).__init__(parent=parent)
        self.__plotItem = None
        self.__scan = None

        self.__label = qt.QLabel(self)
        layout = qt.QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.__label)

    def plotItem(self):
        plotItem = self.__plotItem
        if plotItem is None:
            return None
        return plotItem()

    def setPlotItem(self, plotItem: plot_item_model.ImageItem):
        self.__plotItem = weakref.ref(plotItem)
        self.updateData()

    def scan(self):
        return self.__scan

    def setScan(self, scan: scan_model.Scan):
        self.__scan = scan
        self.updateData()

    def updateData(self):
        scan = self.__scan
        plotItem = self.plotItem()
        if scan is None or plotItem is None:
            self.__label.setText("-")
            return

        imageChannel = plotItem.imageChannel()
        if imageChannel is None:
            self.__label.setText("")
            return

        data = imageChannel.data(self.__scan)
        if data is None:
            self.__label.setText("")
            return

        array = data.array()
        if array is None:
            self.__label.setText("")
            return

        dtype = array.dtype
        self.__label.setText(str(dtype))


class _ImageStatistics(qt.QWidget):
    def __init__(self, parent):
        super(_ImageStatistics, self).__init__(parent=parent)
        self.__plotItem = None
        self.__scan = None

        self.__min = qt.QLabel(self)
        self.__max = qt.QLabel(self)
        self.__mean = qt.QLabel(self)
        self.__std = qt.QLabel(self)

        layout = qt.QFormLayout(self)
        layout.addRow("Min", self.__min)
        layout.addRow("Max", self.__max)
        layout.addRow("Mean", self.__mean)
        layout.addRow("Std", self.__std)
        layout.setContentsMargins(0, 0, 0, 0)

    def plotItem(self):
        plotItem = self.__plotItem
        if plotItem is None:
            return None
        return plotItem()

    def setPlotItem(self, plotItem: plot_item_model.ImageItem):
        self.__plotItem = weakref.ref(plotItem)
        self.updateData()

    def scan(self):
        return self.__scan

    def setScan(self, scan: scan_model.Scan):
        self.__scan = scan
        self.updateData()

    def clearDisplay(self):
        self.__min.setText("")
        self.__max.setText("")
        self.__mean.setText("")
        self.__std.setText("")

    def updateData(self):
        scan = self.__scan
        plotItem = self.plotItem()
        if scan is None or plotItem is None:
            self.clearDisplay()
            return

        imageChannel = plotItem.imageChannel()
        if imageChannel is None:
            self.clearDisplay()
            return

        data = imageChannel.data(self.__scan)
        if data is None:
            self.clearDisplay()
            return

        array = data.array()
        if array is None:
            self.clearDisplay()
            return

        self.__updateDataArray(array)

    def __updateDataArray(self, array):
        result = min_max(array, min_positive=True, finite=True)

        def formatter(v):
            import numbers

            if isinstance(v, numbers.Integral):
                return str(v)
            else:
                return f"{v:.3f}"

        self.__min.setText(formatter(result.minimum))
        self.__max.setText(formatter(result.maximum))
        self.__mean.setText(formatter(numpy.nanmean(array)))
        self.__std.setText(formatter(numpy.nanstd(array)))
        # result.min_positive)
        # result.argmin
        # result.argmin_positive
        # result.argmax


class _LayerField(qt.QLabel):
    def __init__(self, parent=None):
        qt.QLabel.__init__(self, parent=parent)
        self.setToolTip("mock")

    def setLayer(self, layer):
        self.__layer = layer
        if layer is None:
            self.setText("No data")
            self.setToolTip("")
        else:
            if layer.scanId:
                text = f"Scan #{layer.scanId}"
            else:
                text = " × ".join(reversed([str(s) for s in layer.array.shape]))
            self.setText(text)

            msg = ""
            if layer.scanId:
                msg += f"<li><b>Scan id</b>: #{layer.scanId}</li>"
            if layer.scanTitle:
                msg += f"<li><b>Scan title</b>: {layer.scanTitle}</li>"
            msg += f"<li><b>Exposure time</b>: {layer.exposureTime}s</li>"
            self.setToolTip(f"<ul>{msg}</ul>")

    def toolTip(self):
        return """FFFF"""


class _FlatFieldCorrectionPanel(qt.QWidget):
    def __init__(self, parent):
        super(_FlatFieldCorrectionPanel, self).__init__(parent=parent)
        self.__ffc = None

        self.__flat = _LayerField(self)
        self.__dark = _LayerField(self)

        layout = qt.QFormLayout(self)
        layout.addRow("Flat", self.__flat)
        layout.addRow("Dark", self.__dark)
        layout.setContentsMargins(0, 0, 0, 0)

    def createTitle(self, parent):
        widget = qt.QWidget(parent)
        title = qt.QLabel(parent)
        title.setText("Flat field")
        enable = CogCheckBox(parent)
        enable.toggled.connect(self.__changeEnabled)
        enable.setToolTip("Enable/disable the processing")
        layout = qt.QHBoxLayout(widget)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(title)
        layout.addWidget(enable)
        layout.addStretch()
        self.__enable = enable
        return widget

    def __changeEnabled(self, enable):
        ffc = self._ffc()
        ffc.setEnabled(enable)

    def _ffc(self):
        ffc = self.__ffc
        if ffc:
            return ffc()
        self.__ffc = None
        return None

    def setFocusWidget(self, widget):
        ffc = self._ffc()
        if ffc:
            ffc.valueUpdated.disconnect(self.__updateWidget)
        ffc = widget.flatFieldCorrection()
        self.__ffc = weakref.ref(ffc)
        ffc.valueUpdated.connect(self.__updateWidget)
        self.__updateWidget()

    def __updateWidget(self):
        ffc = self._ffc()
        self.__flat.setLayer(ffc.flat())
        self.__dark.setLayer(ffc.dark())
        self.__enable.setChecked(ffc.isEnabled())


class _MaskField(qt.QWidget):
    def __init__(self, parent):
        super(_MaskField, self).__init__(parent=parent)
        self.__correction = None
        self.__label = qt.QLabel(self)
        self.__display = EyeCheckBox(self)
        self.__display.setToolTip("Display the mask as an overlay")
        self.__display.toggled.connect(self.__checked)

        self.__load = qt.QToolButton(self)
        self.__load.setAutoRaise(True)
        self.__load.setToolTip("Load a mask from a file")
        icon = icons.getQIcon("flint:icons/roi-load")
        self.__load.setIcon(icon)
        self.__load.clicked.connect(self.__loadMask)

        self.__remove = qt.QToolButton(self)
        self.__remove.setAutoRaise(True)
        self.__remove.setToolTip("Remove the actual mask")
        icon = icons.getQIcon("flint:icons/remove-item-small")
        self.__remove.setIcon(icon)
        self.__remove.clicked.connect(self.__removeMask)

        layout = qt.QHBoxLayout(self)
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.__label)
        layout.addSpacing(4)
        layout.addStretch(1)
        layout.addWidget(self.__remove)
        layout.addWidget(self.__load)
        layout.addWidget(self.__display)

    def __removeMask(self):
        correction = self._correction()
        correction.setMask(None)

    def __loadMask(self):
        correction = self._correction()
        correction.requestMaskFile()

    def _correction(self):
        correction = self.__correction
        if correction:
            return correction()
        self.__correction = None
        return None

    def correctionWasUpdated(self, correction):
        self.__correction = weakref.ref(correction)
        self.__updateWidget()

    def __updateWidget(self):
        correction = self._correction()
        mask = correction.mask()
        if mask is not None:
            maskShape = " × ".join(reversed([str(s) for s in mask.shape]))
        self.__label.setText(maskShape if mask is not None else "No data")
        self.__remove.setVisible(correction.mask() is not None)

    def __checked(self, isChecked):
        correction = self._correction()
        correction.setMaskDisplayedAsLayer(isChecked)


class _MaskCorrectionPanel(qt.QWidget):
    def __init__(self, parent):
        super(_MaskCorrectionPanel, self).__init__(parent=parent)
        self.__correction = None

        self.__mask = _MaskField(self)

        layout = qt.QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.__mask)

    def createTitle(self, parent):
        widget = qt.QWidget(parent)
        title = qt.QLabel(parent)
        title.setText("Mask")
        enable = CogCheckBox(parent)
        enable.toggled.connect(self.__changeEnabled)
        enable.setToolTip("Enable/disable the processing")
        layout = qt.QHBoxLayout(widget)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(title)
        layout.addWidget(enable)
        layout.addStretch()
        self.__enable = enable
        return widget

    def __changeEnabled(self, enable):
        mask = self._correction()
        mask.setEnabled(enable)

    def _correction(self):
        correction = self.__correction
        if correction:
            return correction()
        self.__correction = None
        return None

    def setFocusWidget(self, widget):
        correction = self._correction()
        if correction:
            correction.valueUpdated.disconnect(self.__updateWidget)
        correction = widget.maskCorrection()
        self.__correction = weakref.ref(correction)
        correction.valueUpdated.connect(self.__updateWidget)
        self.__updateWidget()

    def __updateWidget(self):
        correction = self._correction()
        self.__mask.correctionWasUpdated(correction)
        self.__enable.setChecked(correction.isEnabled())


class _RoiField(qt.QLabel):
    def __init__(self, parent):
        super(_RoiField, self).__init__(parent=parent)
        self.__plotItem = None

        self.__label = qt.QLabel(self)
        self.__icon = IconWidget(self)
        self.__display = EyeCheckBox(self)
        self.__display.toggled.connect(self.__checked)
        layout = qt.QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)

        self.__icon.setIconSize(16, 16)
        self.__icon.setIconName("flint:icons/device-image-roi")

        layout.addWidget(self.__icon)
        layout.addWidget(self.__label)
        layout.addStretch(1)
        layout.addWidget(self.__display)

    def plotItem(self):
        plotItem = self.__plotItem
        if plotItem is None:
            return None
        return plotItem()

    def __checked(self, isChecked):
        plotItem = self.plotItem()
        if plotItem is None:
            return
        plotItem.setVisible(isChecked)

    def setPlotItem(self, plotItem: plot_item_model.RoiItem):
        if plotItem is None:
            self.__label.setText("")
            self.setEnabled(False)
            return

        roiName = plotItem.name()
        if roiName is None:
            self.__label.setText("")
            self.setEnabled(False)
            return

        self.__plotItem = weakref.ref(plotItem)

        self.__display.setChecked(plotItem.isVisible())

        self.setEnabled(True)
        self.__label.setText(roiName)


class ImagePlotProperty2Widget(qt.QWidget):
    def __init__(self, parent=None):
        super(ImagePlotProperty2Widget, self).__init__(parent=parent)
        self.__scan: Optional[scan_model.Scan] = None
        self.__flintModel: Optional[flint_model.FlintState] = None
        self.__plotModel: Optional[plot_model.Plot] = None

        self.__focusWidget = None

        self.__aggregator = plot_helper.PlotEventAggregator(self)
        self.__refreshManager = refresh_helper.RefreshManager(self)
        # self.__refreshManager.refreshModeChanged.connect(self.__refreshModeChanged)
        self.__refreshManager.setAggregator(self.__aggregator)

        self.__content = self.__createLayout(self)
        toolBar = self.__createToolBar()

        self.setAutoFillBackground(True)
        line = qt.QFrame(self)
        line.setFrameShape(qt.QFrame.HLine)
        line.setFrameShadow(qt.QFrame.Sunken)

        scroll = qt.QScrollArea(self)
        scroll.setBackgroundRole(qt.QPalette.Light)
        scroll.setFrameShape(qt.QFrame.NoFrame)
        scroll.setWidgetResizable(True)
        scroll.setWidget(self.__content)

        layout = qt.QVBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        layout.addWidget(toolBar)
        layout.addWidget(line)
        layout.addWidget(scroll)

    def __createLayout(self, parent):
        content = qt.QWidget(parent)
        layout = FormGridLayout(content)
        layout.setColumnStretch(0, 0)
        layout.setColumnStretch(1, 1)

        self.__deviceName = _DeviceField(content)
        self.__channelName = _ImageField(content)
        self.__imageSize = _ImageSizeField(content)
        self.__imageDType = _ImageDTypeField(content)
        self.__imageState = _ImageStatistics(content)
        self.__imageFFC = _FlatFieldCorrectionPanel(content)
        self.__imageMask = _MaskCorrectionPanel(content)
        self.__rois = qt.QWidget(content)
        roiLayout = qt.QVBoxLayout(self.__rois)
        roiLayout.setContentsMargins(0, 0, 0, 0)

        layout.addRow("Device", self.__deviceName)
        layout.addRow("Channel name", self.__channelName)
        layout.addRow("Image size", self.__imageSize)
        layout.addRow("Image dtype", self.__imageDType)

        self.__roiSeparator = qt.QFrame(self)
        self.__roiSeparator.setFrameShape(qt.QFrame.HLine)
        self.__roiSeparator.setFrameShadow(qt.QFrame.Sunken)
        layout.addRow(self.__roiSeparator)
        self.__roiTitle = qt.QLabel(self)
        self.__roiTitle.setText("ROIs")
        layout.addRow(self.__roiTitle, self.__rois)

        self.__ffcSeparator = qt.QFrame(self)
        self.__ffcSeparator.setFrameShape(qt.QFrame.HLine)
        self.__ffcSeparator.setFrameShadow(qt.QFrame.Sunken)
        layout.addRow(self.__ffcSeparator)
        self.__ffcTitle = self.__imageFFC.createTitle(self)
        layout.addRow(self.__ffcTitle, self.__imageFFC)
        self.__toggleFlatFieldAction(False)

        self.__maskSeparator = qt.QFrame(self)
        self.__maskSeparator.setFrameShape(qt.QFrame.HLine)
        self.__maskSeparator.setFrameShadow(qt.QFrame.Sunken)
        layout.addRow(self.__maskSeparator)
        self.__maskTitle = self.__imageMask.createTitle(self)
        layout.addRow(self.__maskTitle, self.__imageMask)
        self.__toggleMaskAction(False)

        self.__stateSeparator = qt.QFrame(self)
        self.__stateSeparator.setFrameShape(qt.QFrame.HLine)
        self.__stateSeparator.setFrameShadow(qt.QFrame.Sunken)
        layout.addRow(self.__stateSeparator)
        self.__stateTitle = qt.QLabel(self)
        self.__stateTitle.setText("Statistics")
        layout.addRow(self.__stateTitle, self.__imageState)

        vstretch = qt.QSpacerItem(
            0, 0, qt.QSizePolicy.Minimum, qt.QSizePolicy.Expanding
        )
        layout.addItem(vstretch)
        return content

    def __createToolBar(self):
        toolBar = qt.QToolBar(self)
        toolBar.setMovable(False)

        flatFieldAction = qt.QAction(self)
        flatFieldAction.setCheckable(True)
        flatFieldAction.setText("Enable flat field correction")
        flatFieldAction.toggled.connect(self.__toggleFlatFieldAction)
        icon = icons.getQIcon("flint:icons/data-flat-field")
        flatFieldAction.setIcon(icon)
        toolBar.addAction(flatFieldAction)

        maskAction = qt.QAction(self)
        maskAction.setCheckable(True)
        maskAction.setText("Enable mask correction")
        maskAction.toggled.connect(self.__toggleMaskAction)
        icon = icons.getQIcon("flint:icons/data-mask")
        maskAction.setIcon(icon)
        toolBar.addAction(maskAction)

        return toolBar

    def __toggleFlatFieldAction(self, checked):
        if self.__focusWidget:
            self.__focusWidget.flatFieldCorrection().setEnabled(checked)
        self.__imageFFC.setVisible(checked)
        self.__ffcSeparator.setVisible(checked)
        self.__ffcTitle.setVisible(checked)

    def __toggleMaskAction(self, checked):
        if self.__focusWidget:
            self.__focusWidget.maskCorrection().setEnabled(checked)
        self.__imageMask.setVisible(checked)
        self.__maskSeparator.setVisible(checked)
        self.__maskTitle.setVisible(checked)

    def setFlintModel(self, flintModel: flint_model.FlintState = None):
        self.__flintModel = flintModel

    def focusWidget(self):
        return self.__focusWidget

    def setFocusWidget(self, widget):
        if self.__focusWidget is not None:
            widget.plotModelUpdated.disconnect(self.__plotModelUpdated)
            widget.scanModelUpdated.disconnect(self.__currentScanChanged)
        self.__focusWidget = widget
        if self.__focusWidget is not None:
            widget.plotModelUpdated.connect(self.__plotModelUpdated)
            widget.scanModelUpdated.connect(self.__currentScanChanged)
            plotModel = widget.plotModel()
            scanModel = widget.scan()
        else:
            plotModel = None
            scanModel = None
        self.__plotModelUpdated(plotModel)
        self.__currentScanChanged(scanModel)
        self.__imageFFC.setFocusWidget(widget)
        self.__imageMask.setFocusWidget(widget)

    def __plotModelUpdated(self, plotModel):
        self.setPlotModel(plotModel)

    def setPlotModel(self, plotModel: plot_model.Plot):
        if self.__plotModel is not None:
            self.__plotModel.structureChanged.disconnect(self.__structureChanged)
            self.__plotModel.itemValueChanged.disconnect(self.__itemValueChanged)
        self.__plotModel = plotModel
        if self.__plotModel is not None:
            self.__plotModel.structureChanged.connect(self.__structureChanged)
            self.__plotModel.itemValueChanged.connect(self.__itemValueChanged)
        self.__updateDisplay()

    def __currentScanChanged(self, scanModel):
        self.__setScan(scanModel)

    def __structureChanged(self):
        self.__updateDisplay()

    def __itemValueChanged(
        self, item: plot_model.Item, eventType: plot_model.ChangeEventType
    ):
        pass

    def plotModel(self) -> Union[None, plot_model.Plot]:
        return self.__plotModel

    def __setScan(self, scan: scan_model.Scan = None):
        if self.__scan is scan:
            return
        if self.__scan is not None:
            self.__scan.scanDataUpdated[object].disconnect(
                self.__aggregator.callbackTo(self.__scanDataUpdated)
            )
            self.__scan.scanStarted.disconnect(
                self.__aggregator.callbackTo(self.__scanStarted)
            )
            self.__scan.scanFinished.disconnect(
                self.__aggregator.callbackTo(self.__scanFinished)
            )
        self.__scan = scan
        # As the scan was updated, clear the previous cached events
        self.__aggregator.clear()
        self.__imageSize.setScan(scan)
        self.__imageDType.setScan(scan)
        self.__imageState.setScan(scan)
        if self.__scan is not None:
            self.__scan.scanDataUpdated[object].connect(
                self.__aggregator.callbackTo(self.__scanDataUpdated)
            )
            self.__scan.scanStarted.connect(
                self.__aggregator.callbackTo(self.__scanStarted)
            )
            self.__scan.scanFinished.connect(
                self.__aggregator.callbackTo(self.__scanFinished)
            )
        self.__updateDisplay()

    def __scanStarted(self):
        pass

    def __scanFinished(self):
        pass

    def __scanDataUpdated(self, event: scan_model.ScanDataUpdateEvent):
        plotModel = self.__plotModel
        if plotModel is None:
            return
        self.__imageSize.updateData()
        self.__imageDType.updateData()
        self.__imageState.updateData()

    def __updateDisplay(self):
        if self.__plotModel is None:
            return

        roisLayout = self.__rois.layout()
        for i in reversed(range(roisLayout.count())):
            roisLayout.itemAt(i).widget().deleteLater()

        countImages = 0
        countRois = 0
        for plotItem in self.__plotModel.items():
            if isinstance(plotItem, plot_item_model.ImageItem):
                countImages += 1
                self.__channelName.setPlotItem(plotItem)
                self.__deviceName.setPlotItem(plotItem)
                self.__imageSize.setPlotItem(plotItem)
                self.__imageDType.setPlotItem(plotItem)
                self.__imageState.setPlotItem(plotItem)

            elif isinstance(plotItem, plot_item_model.RoiItem):
                countRois += 1
                roi = _RoiField(self.__rois)
                roisLayout.addWidget(roi)
                roi.setPlotItem(plotItem)

        self.__rois.setVisible(countRois > 0)
        self.__roiSeparator.setVisible(countRois > 0)
        self.__roiTitle.setVisible(countRois > 0)

        if countImages == 0:
            self.__channelName.setPlotItem(None)
            self.__deviceName.setPlotItem(None)
        elif countImages >= 2:
            _logger.warning(
                "More than one image is provided by this plot. A single image is supported, others will be ignored."
            )
