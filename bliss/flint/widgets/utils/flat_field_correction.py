# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy
import typing
import functools
import logging
from silx.gui import qt
from bliss.flint.helper import scan_info_helper
from bliss.flint.model import scan_model


_logger = logging.getLogger(__name__)


def extract_exposure_time(scan_info: dict) -> typing.Tuple[str, float]:
    technique = scan_info.get("technique", {})

    def read_exposure_time_in_second(meta):
        value = meta["exposure_time"]
        unit = scan.get("exposure_time@units", "s")
        coefs = {"s": 1, "ms": 0.001}
        if unit not in coefs:
            raise RuntimeError(f"Unsupported exposure_time unit '{unit}' in scan")
        return value * coefs[unit]

    if "dark" in technique:
        scan = technique["dark"]
        tomo_scan = "dark"
        exposure_time = read_exposure_time_in_second(scan)
    elif "flat" in technique:
        scan = technique["flat"]
        tomo_scan = "flat"
        exposure_time = read_exposure_time_in_second(scan)
    elif "proj" in technique:
        scan = technique["proj"]
        tomo_scan = "proj"
        exposure_time = read_exposure_time_in_second(scan)
    elif "scan" in technique and "tomo_n" in technique["scan"]:
        scan = technique["scan"]
        tomo_scan = "proj"
        exposure_time = scan["exposure_time"] / 1000
    else:
        tomo_scan = None
        # NOTE: BLISS 1.9 expose mesh scan npoints as numpy int64
        # cast it to avoid further json serialization fail
        exposure_time = scan_info.get("count_time")
    return tomo_scan, exposure_time


class Layer(typing.NamedTuple):
    array: numpy.ndarray
    exposureTime: float
    scanId: str
    scanTitle: str

    def __hash__(self):
        """Needed to use the lru cache decorator"""
        return id(self)

    @functools.lru_cache(1)
    def normalized(self, shape, exposureTime: float):
        if self.array.shape != shape:
            return None
        tdark = exposureTime / self.exposureTime
        return numpy.asarray(self.array, float) * tdark


class FlatFieldCorrection(qt.QObject):

    valueUpdated = qt.Signal()

    def __init__(self, parent: qt.QObject = None):
        qt.QObject.__init__(self, parent=parent)
        self.__dark: typing.Optional[Layer] = None
        self.__flat: typing.Optional[Layer] = None
        self.__isEnabled = False

    def setEnabled(self, enabled: bool):
        if self.__isEnabled == enabled:
            return
        self.__isEnabled = enabled
        self.valueUpdated.emit()

    def isEnabled(self):
        return self.__isEnabled

    def captureImage(self, scan, image):
        scanInfo = scan.scanInfo()
        tomoScan, exposureTime = extract_exposure_time(scanInfo)
        if tomoScan == "dark":
            self.setDark(image, exposureTime, scan)
        elif tomoScan == "flat":
            self.setFlat(image, exposureTime, scan)

    def dark(self) -> typing.Optional[Layer]:
        return self.__dark

    def setDark(
        self, image: numpy.ndarray, exposureTime: float, scan: scan_model.Scan = None
    ):
        if scan:
            scanInfo = scan.scanInfo()
            title = scan_info_helper.get_full_title(scan)
            scanId = scanInfo.get("scan_nb", None)
        else:
            title = None
            scanId = None
        self.__dark = Layer(image, exposureTime, scanId, title)
        self.valueUpdated.emit()

    def flat(self) -> typing.Optional[Layer]:
        return self.__flat

    def setFlat(
        self, image: numpy.ndarray, exposureTime: float, scan: scan_model.Scan = None
    ):
        if scan:
            scanInfo = scan.scanInfo()
            title = scan_info_helper.get_full_title(scan)
            scanId = scanInfo.get("scan_nb", None)
        else:
            title = None
            scanId = None
        self.__flat = Layer(image, exposureTime, scanId, title)
        self.valueUpdated.emit()

    def correction(self, array: numpy.ndarray, exposureTime: float):
        if self.__dark is None and self.__flat is None:
            return array

        def normalized(layer: Layer):
            if layer is None:
                return None
            shape = tuple(array.shape)
            return layer.normalized(shape, exposureTime)

        flat = normalized(self.__flat)
        dark = normalized(self.__dark)
        if dark is None and flat is None:
            return array

        if dark is None:
            with numpy.errstate(divide="ignore"):
                data = array / flat
            data[numpy.logical_not(numpy.isfinite(data))] = 0
            return data

        if flat is None:
            return array - dark

        with numpy.errstate(divide="ignore"):
            data = (array - dark) / (flat - dark)
        data[numpy.logical_not(numpy.isfinite(data))] = 0
        return data
